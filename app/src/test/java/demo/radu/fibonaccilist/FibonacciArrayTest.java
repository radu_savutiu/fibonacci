package demo.radu.fibonaccilist;

import junit.framework.TestCase;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by Radu on 10/27/2015.
 */
public class FibonacciArrayTest extends TestCase {

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
    }

    public void testCorrectness() {
        System.out.println("testCorrectness");
        FibonacciArray.calculateAllFibonacciNumbersSmallerThan(BigInteger.valueOf(4000));
        List<BigInteger> res = FibonacciArray.getResults();
        for (int i = 2; i < res.size(); i++) {
            assertEquals(res.get(i), res.get(i - 1).add(res.get(i - 2)));
        }
    }

    public void testSpeed4000() {
        System.out.println("testSpeed4000");
        speedTest(BigInteger.valueOf(4000));
    }

    public void testSpeed10000000() {
        System.out.println("testSpeed10000000");
        BigInteger N = BigInteger.valueOf(10000000);
        speedTest(N);
        speedTest2(N);
        speedTest(BigInteger.TEN);
    }

    public void testSpeedBigNumber() {
        System.out.println("testSpeedBigNumber");
        speedTest(new BigInteger("99999999999999999999999999999999999999999999"));
        speedTest2(new BigInteger("99999999999999999999999999999999999999999999"));
    }

    private void speedTest(BigInteger N) {
        FibonacciArray.resetResults();
        long startTime = System.nanoTime();
        FibonacciArray.calculateAllFibonacciNumbersSmallerThan(N);
        long stopTime = System.nanoTime();
        System.out.printf("Time elapsed during calcs up to %d is: %d\n", N, (stopTime - startTime));

        startTime = System.nanoTime();
        FibonacciArray.calculateAllFibonacciNumbersSmallerThan(N);
        stopTime = System.nanoTime();
        System.out.printf("Time elapsed with optimization up to %d is: %d\n", N, (stopTime - startTime));
    }

    private void speedTest2(BigInteger N) {
        FibonacciArray.resetResults();
        long startTime = System.nanoTime();
        FibonacciArray.calculateAllFibonacciNumbersSmallerThan(N);
        long stopTime = System.nanoTime();
        System.out.printf("Time elapsed during calcs up to %d is: %d\n", N, (stopTime - startTime));

        startTime = System.nanoTime();
        FibonacciArray.calculateAllFibonacciNumbersSmallerThan(N.divide(BigInteger.valueOf(2)));
        stopTime = System.nanoTime();
        System.out.printf("Time elapsed with optimization up to %d / 2  is: %d\n", N, (stopTime-startTime));
    }
}