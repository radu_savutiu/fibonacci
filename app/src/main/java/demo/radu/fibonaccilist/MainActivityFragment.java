package demo.radu.fibonaccilist;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {
    public static String ACTION_CALCULATION_COMPLETE = "CALCULATION_COMPLETE";
    public static String EXTRA_BIGGEST_INDEX_NEEDED = "EXTRA_BIGGEST_INDEX_NEEDED";

    @Bind(R.id.list)
    RecyclerView mRecyclerView;
    List<BigInteger> fibonacciList;

    private static final String TAG = "MainActivityFragment";
    private NumberAdapter mAdapter;
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_CALCULATION_COMPLETE) &&
                    intent.hasExtra(EXTRA_BIGGEST_INDEX_NEEDED)) {
                int maxIndex = intent.getIntExtra(EXTRA_BIGGEST_INDEX_NEEDED, 0);
                if (maxIndex!=0) {
                    fibonacciList = FibonacciArray.getResults().subList(0, maxIndex);
                    mAdapter.setData(fibonacciList);
                    mAdapter.notifyDataSetChanged();
                }
            }
        }
    };

    public MainActivityFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mReceiver,
                new IntentFilter(ACTION_CALCULATION_COMPLETE));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View parentView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, parentView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        if (fibonacciList==null) {
            fibonacciList = FibonacciArray.getResults();
        }
        mAdapter = new NumberAdapter(fibonacciList);
        mRecyclerView.setAdapter(mAdapter);


        return parentView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            showFibonacciDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showFibonacciDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.title);

        // Set up the input
        final EditText input = new EditText(getActivity());
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    String newValue = input.getText().toString();
                    Log.i(TAG, "N:" + newValue);

                    generateNewList(newValue);
                } catch (Exception e) {
                    Snackbar.make(mRecyclerView, e.getMessage(), Snackbar.LENGTH_LONG).show();
                }
                ;
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void generateNewList(String valueToCalculateUpTo) {
        FibonacciIntentService.startActionCalculate(getActivity(), valueToCalculateUpTo);
    }
}
