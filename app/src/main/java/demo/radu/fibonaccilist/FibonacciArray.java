package demo.radu.fibonaccilist;

import android.util.Log;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Radu on 10/27/2015.
 */
public class FibonacciArray {
    private static final String TAG = "TAG";
    private static List<BigInteger> results = new ArrayList<>();

    public static void resetResults() {
        results = new ArrayList<>();
        results.add(BigInteger.ONE);
        results.add(BigInteger.ONE);
    }

    public static List<BigInteger> getResults() {
        if (results.isEmpty()) {
            results.add(BigInteger.ONE);
            results.add(BigInteger.ONE);
        }
        return results;
    }

    public static int calculateAllFibonacciNumbersSmallerThan(BigInteger MAX) {
        if (MAX.equals(BigInteger.ZERO)) {
            return 2;
        }
        if (results.isEmpty()) {
            results.add(BigInteger.ONE);
            results.add(BigInteger.ONE);
        }
        final int initialLength = results.size();
        int finalLen = initialLength;

        if (results.get(initialLength - 1).compareTo(MAX)==0) {
            return finalLen;
        } else if (results.get(initialLength - 1).compareTo(MAX)==1) {
            finalLen = initialLength - 1;
            while (results.get(finalLen).compareTo(MAX)==1) {
                finalLen--;
            }
            getResults();
            return finalLen + 1;
        } else {
            BigInteger a;
            do {
                a = getNthFibonacciNumber(++finalLen);
            }
            while ((a.compareTo(MAX)<1));
            if (results.get(finalLen-1).compareTo(MAX)==1) {
                return finalLen - 1;
            }
            return finalLen;

        }
    }

    private static BigInteger getNthFibonacciNumber(Integer N) {
        if (N <= 1) { // if
            return BigInteger.valueOf(1);
        }
        if (results.size() > N && results.get(N) != null) {
            return results.get(N);
        } else {
            BigInteger v = getNthFibonacciNumber(N - 1).add(getNthFibonacciNumber(N - 2));
            results.add(v);
            return v;
        }
    }
}
