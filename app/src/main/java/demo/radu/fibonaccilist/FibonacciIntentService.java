package demo.radu.fibonaccilist;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.support.v4.content.LocalBroadcastManager;

import java.math.BigInteger;
import java.util.List;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class FibonacciIntentService extends IntentService {
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_CALCULATE = "demo.radu.fibonaccilist.action.CALCULATE";
    private static final String EXTRA_PARAM_N = "demo.radu.fibonaccilist.extra.PARAM_N";

    public static void startActionCalculate(Context context, String N) {
        Intent intent = new Intent(context, FibonacciIntentService.class);
        intent.setAction(ACTION_CALCULATE);
        intent.putExtra(EXTRA_PARAM_N, N);
        context.startService(intent);
    }

    public FibonacciIntentService() {
        super("FibonacciIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_CALCULATE.equals(action)) {
                final String valueToCalculateUpTo = intent.getStringExtra(EXTRA_PARAM_N);
                BigInteger N = new BigInteger(valueToCalculateUpTo);
                int maxIndex = FibonacciArray.calculateAllFibonacciNumbersSmallerThan(N);
                Intent i = new Intent(MainActivityFragment.ACTION_CALCULATION_COMPLETE);
                i.putExtra(MainActivityFragment.EXTRA_BIGGEST_INDEX_NEEDED, maxIndex);
                LocalBroadcastManager.getInstance(this).sendBroadcast(i);
            }
        }
    }
}
