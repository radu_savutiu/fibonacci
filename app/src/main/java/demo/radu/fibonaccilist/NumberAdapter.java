package demo.radu.fibonaccilist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.math.BigInteger;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Radu on 10/23/2015.
 */
public class NumberAdapter extends RecyclerView.Adapter<NumberAdapter.ViewHolder> {
    private List<BigInteger> numberList;

    public NumberAdapter(List<BigInteger> list) {
        numberList = list;
    }

    public void setData(List<BigInteger> data) {
        numberList = data;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        @Bind(R.id.tvNumber)
        TextView tvNumber;

        public ViewHolder(View parentView) {
            super(parentView);
            ButterKnife.bind(this, parentView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parentViewGroup, int i) {
        // create a new view
        View v = LayoutInflater.from(parentViewGroup.getContext())
                .inflate(R.layout.item_layout, parentViewGroup, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final NumberAdapter.ViewHolder viewHolder, int position) {
        viewHolder.tvNumber.setText(numberList.get(position).toString());
    }

    @Override
    public int getItemCount() {
        return numberList.size();
    }
}
